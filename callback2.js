/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

function listInfo(list, boardId, callback) {
  if (arguments.length < 3 || typeof list !== "object") {
    throw new Error("Invalid Input");
  }

  setTimeout(() => {
    for (let key in list) {
      if (key === boardId) {
        callback(list[key]);
        return;
      }
    }
    console.log("Invalid Data");
  }, 2 * 1000);
}

module.exports = listInfo;
