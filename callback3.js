/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

function cardsInfo(cards, list, board, callback) {
  if (arguments.length < 3 || typeof cards !== "object") {
    throw new Error("Invalid Input");
  }
  let boardId = board.map((obj) => obj.id);
  boardId.forEach((id) => {
    if (list.hasOwnProperty(id)) {
      let listId = list[id].map((obj) => obj.id);
      setTimeout(() => {
        listId.forEach((listofId) => {
          if (cards.hasOwnProperty(listofId)) {
            callback(cards[listofId], listofId);
          }
        });
      }, 2 * 1000);
    }
  });
}

module.exports = cardsInfo;
