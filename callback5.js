/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const boardInfo = require("./callback1");
const listsInfo = require("./callback2");
const cardsInfo = require("./callback3");

function infoThanosWithCardsOfMindsAndLists(
  board,
  list,
  cards,
  boardName,
  listName1,
  listName2
) {
  if (
    arguments.length < 6 ||
    !Array.isArray(board) ||
    typeof list !== "object" ||
    typeof cards !== "object"
  ) {
    throw new Error("Invalid Input");
  }
  setTimeout(() => {
    let boardId;
    let listId1;
    let listId2;

    for (let index in board) {
      if (board[index].name === boardName) {
        boardId = board[index].id;
        break;
      }
    }
    for (let [key, value] of Object.entries(list)) {
      for (let element in value) {
        if (value[element].name === listName1) {
          listId1 = value[element].id;
        }
        if (value[element].name === listName2) {
          listId2 = value[element].id;
        }
      }
    }
    let listArray = [listId1, listId2];
    boardInfo(board, boardId, (boardData) => {
      console.log(boardData);
      listsInfo(list, boardId, (listData) => {
        console.log(listData);

        for (let index in listArray) {
          cardsInfo(cards, list, board, (cardsData, id) => {
            if (listArray[index] === id) {
              console.log(cardsData);
            }
          });
        }
      });
    });
  }, 2000);
}

module.exports = infoThanosWithCardsOfMindsAndLists;
