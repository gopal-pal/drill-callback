/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const boardInfo = require("./callback1");
const listInfo = require("./callback2");
const cardsInfo = require("./callback3");

function boardsAndListsOfThanosWithCards(
  board,
  list,
  cards,
  boardName,
  listArray
) {
  if (
    arguments.length < 5 ||
    !Array.isArray(board) ||
    typeof list !== "object" ||
    typeof cards !== "object" ||
    !Array.isArray(listArray)
  ) {
    throw new Error("Invalid Input");
  }
  setTimeout(() => {
    let boardId;

    for (let index in board) {
      if (board[index].name === boardName) {
        boardId = board[index].id;
      }
    }
    boardInfo(board, boardId, (boardData) => {
      console.log(boardData);
      listInfo(list, boardId, (listData) => {
        console.log(listData);

        for (let index in listArray) {
          cardsInfo(cards, list, board, (cardsData, id) => {
            if (listArray[index] === id) {
              console.log(cardsData);
            }
          });
        }
      });
    });
  }, 2 * 1000);
}

module.exports = boardsAndListsOfThanosWithCards;
