/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from
the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const boardInfo = require("./callback1");
const listInfo = require("./callback2");
const cardsInfo = require("./callback3");

function boardAndCardsInfoOfThomas(board, list, cards, boardName, listName) {
  if (
    arguments.length < 5 ||
    !Array.isArray(board) ||
    typeof list !== "object" ||
    typeof cards !== "object"
  ) {
    throw new Error("Invalid Input");
  }
  setTimeout(() => {
    let boardId;
    let listId;

    for (let index in board) {
      if (board[index].name === boardName) {
        boardId = board[index].id;
        break;
      }
    }
    for (let [key, value] of Object.entries(list)) {
      for (let element in value) {
        if (value[element].name === listName) {
          listId = value[element].id;
          break;
        }
      }
    }
    boardInfo(board, boardId, (boardData) => {
      console.log(boardData);
      listInfo(list, boardId, (listData) => {
        console.log(listData);
        cardsInfo(cards, list, board, (cardData, id) => {
          if (listId === id) {
            console.log(cardData);
          }
        });
      });
    });
  }, 2 * 1000);
}

module.exports = boardAndCardsInfoOfThomas;
