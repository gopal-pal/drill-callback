const boardsAndListsOfThanosWithCards = require("../callback6");

const board = require("../Data/boards.json");
const list = require("../Data/lists.json");
const cards = require("../Data/cards.json");

let keys = Object.keys(cards);

let listArray = [];

for (let [key, value] of Object.entries(list)) {
  for (let element in value) {
    if (keys.includes(value[element].id)) {
      listArray.push(value[element].id);
    }
  }
}

try {
  boardsAndListsOfThanosWithCards(board, list, cards, "Thanos", listArray);
} catch (err) {
  console.log(err.message);
}
