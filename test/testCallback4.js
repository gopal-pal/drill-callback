const boardAndCardsInfoOfThomas = require("../callback4");

const board = require("../Data/boards.json");
const list = require("../Data/lists.json");
const cards = require("../Data/cards.json");

try {
  boardAndCardsInfoOfThomas(board, list, cards, "Thanos", "Mind");
} catch (err) {
  console.log(err.message);
}
