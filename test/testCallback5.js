const infoThanosWithCardsOfMindsAndLists = require("../callback5");

const board = require("../Data/boards.json");
const list = require("../Data/lists.json");
const cards = require("../Data/cards.json");

try {
  infoThanosWithCardsOfMindsAndLists(
    board,
    list,
    cards,
    "Thanos",
    "Mind",
    "Space"
  );
} catch (err) {
  console.log(err.message);
}
