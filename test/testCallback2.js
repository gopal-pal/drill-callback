const list = require("../Data/lists.json");
const board = require("../Data/boards.json");
const listInfo = require("../callback2");

let boardId = board.map((obj) => obj.id);
boardId.forEach((id) => {
  try {
    listInfo(list, id, (data) => console.log(data));
  } catch (err) {
    console.log(err.message);
  }
});
