/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

function boardInfo(board, boardId, callback) {
  if (arguments.length < 3 || !Array.isArray(board)) {
    throw new Error("Invalid Input");
  }

  setTimeout(() => {
    for (let i in board) {
      if (board[i].id === boardId) {
        callback(board[i]);
        return;
      }
    }
    console.log("Invalid Data");
  }, 2 * 1000);
}

module.exports = boardInfo;
